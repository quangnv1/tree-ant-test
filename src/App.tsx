import { EditOutlined, PlusOutlined } from "@ant-design/icons";
import { Col, Row } from "antd";
import React from "react";
import "./App.scss";
import TreeComponent from "./components/DirectoryTree/TreeComponent";

const treeData = [
  {
    title: "Marketing AcheckIn",
    key: "0",
    children: [
      {
        title: "Marketing AcheckIn 1",
        key: "0-1",
        children: [
          {
            title: "Marketing AcheckIn 2",
            key: "0-2",
          },
        ],
      },
    ],
  },
  {
    title: "BA AcheckIn",
    key: "1",
    children: [
      {
        title: "BA AcheckIn 1",
        key: "1-1",
        children: [
          {
            title: "BA AcheckIn 2",
            key: "1-2",
          },
        ],
      },
    ],
  },
  {
    title: "Design AcheckIn",
    key: "2",
    children: [
      {
        title: "Design AcheckIn 1",
        key: "2-1",
        children: [
          {
            title: "Design AcheckIn 2",
            key: "2-2",
          },
        ],
      },
    ],
  },
  {
    title: "Product AcheckIn",
    key: "3",
    children: [
      {
        title: "Client AcheckIn 1",
        key: "3-1",
        children: [
          {
            title: "Client AcheckIn 2",
            key: "3-2",
          },
        ],
      },
      {
        title: "Backend AcheckIn 1",
        key: "4",
        children: [
          {
            title: "Backend AcheckIn 2",
            key: "4-1",
          },
        ],
      },
    ],
  },
];

function App() {
  const onSelect = (selectedKeys: React.Key[], info: any) => {
    console.log(selectedKeys, info);
  };

  return (
    <Row style={{ margin: "30px" }} className="app">
      <Col
        xs={{ span: "24" }}
        lg={{ span: "10" }}
        xl={{ span: "12" }}
        xxl={{ span: 6 }}
        className="checkin"
      >
        <div className="a-checkin">
          <div className="flex">
            <span className="name">A</span>{" "}
            <div className="title">Acheckin</div>
          </div>

          <div className="action flex">
            <div className="mr-15">
              <PlusOutlined className="cursor-pointer" />
            </div>
            <div className="mr-5">
              <EditOutlined className="cursor-pointer" />
            </div>
          </div>
        </div>
        <div className="pl-45">
          <TreeComponent treeData={treeData} onSelect={onSelect} />
        </div>
      </Col>
    </Row>
  );
}

export default App;
