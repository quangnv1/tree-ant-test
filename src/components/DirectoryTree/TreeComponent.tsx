import React from "react";
import Tree, { DataNode } from "antd/lib/tree";
import { DownOutlined, MoreOutlined } from "@ant-design/icons";

const { DirectoryTree } = Tree;

interface Props {
  treeData: any[];
  onSelect: (selectedKeys: React.Key[], info: any) => void;
}

const TreeComponent = (props: Props) => {
  const { treeData, onSelect } = props;

  const onSelectTree = (selectedKeys: React.Key[], info: any) => {
    onSelect(selectedKeys, info);
  };

  return (
    <DirectoryTree
      titleRender={(node: DataNode) => {
        return (
          <div className="tree-data">
            <div>{node.title}</div>
            <div>
              <MoreOutlined />
            </div>
          </div>
        );
      }}
      showIcon={false}
      switcherIcon={<DownOutlined />}
      onSelect={onSelectTree}
      treeData={treeData}
    />
  );
};

export default TreeComponent;
